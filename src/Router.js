export default class Router {
	static titleElement;
	static contentElement;
	static #menuElement;
	/**
	 * Tableau des routes/pages de l'application.
	 * @example `Router.routes = [{ path: '/', page: pizzaList, title: 'La carte' }]`
	 */
	static routes = [];

	/**
	 * Affiche la page correspondant à `path` dans le tableau `routes`
	 * @param {String} path URL de la page à afficher
	 */
	static navigate(path) {
		const route = this.routes.find(route => route.path === path);
		if (route) {
			// affichage du titre de la page
			this.titleElement.innerHTML = `<h1>${route.title}</h1>`;
			// affichage de la page elle même
			this.contentElement.innerHTML = route.page.render();
		}
	}
	static set menuElement(element){
		this.#menuElement = element;
		const tab = this.#menuElement.querySelectorAll('a');
		console.log(tab);
		tab.forEach(values => {
			values.addEventListener('click', event =>{
				event.preventDefault();
				this.navigate(values.getAttribute('href'));
			});
		});
	}
}
