import Router from './Router';
import data from './data';
import PizzaList from './pages/PizzaList';

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');

import Component from './components/Component';

const pizzaList = new PizzaList(data),
	aboutPage = new Component('section', null, 'Ce site est génial'),
	pizzaForm = new Component('section', null, 'Ici vous pourrez ajouter une pizza');

Router.routes = [{ path: '/', page: pizzaList, title: 'La carte' }];

Router.routes = [
	{ path: '/', page: pizzaList, title: 'La carte' },
	{ path: '/a-propos', page: aboutPage, title: 'À propos' },
	{ path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];

Router.navigate('/'); // affiche la liste des pizzas
Router.menuElement = document.querySelector('.mainMenu');


document.querySelector(".logo").innerHTML += "<small>les pizzas c'est la vie</small>"; 
//console.log(document.querySelector('.logo').innerHTML);
document.querySelector('.pizzaListLink').setAttribute('class', ' active');
document.querySelector('section').setAttribute('style', '');

var button = document.querySelector('.closeButton');

button.addEventListener('click', event => {
    document.querySelector('section').setAttribute('style', 'display:none');
})